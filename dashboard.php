<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['employeeid']==0)) {
  header('location:logout.php');
  } else{

  ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ISS IMS - Dashboard</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/datepicker3.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">

    <!--Custom Font-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

</head>

<body>

    <?php include_once('includes/header.php');?>
    <?php include_once('includes/sidebar.php');?>

    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="dashboard.php">
                        <em class="fa fa-home"></em>
                    </a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Dashboard</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6 col-md-3">

                <div class="panel panel-default">
                    <div class="panel-body easypiechart-panel">
                        <?php
//Today Expense
$userid=$_SESSION['employeeid'];
$tdate=date('Y-m-d');
$query=mysqli_query($con,"select sum(ExpenseCost)  as todaysexpense from tblexpense where (ExpenseDate)='$tdate' && (UserId='$userid');");
$result=mysqli_fetch_array($query);
$sum_today_expense=$result['todaysexpense'];
 ?>

                        <h4>Today's Expense</h4>
                        <div class="easypiechart" id="easypiechart-blue"
                            data-percent="<?php echo $sum_today_expense;?>"><span class="percent"><?php if($sum_today_expense==""){
echo "0";
} else {
echo $sum_today_expense;
}

	?></span></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3">
                <div class="panel panel-default">
                    <?php
//Yestreday Expense
$userid=$_SESSION['employeeid'];
$ydate=date('Y-m-d',strtotime("-1 days"));
$query1=mysqli_query($con,"select sum(ExpenseCost)  as yesterdayexpense from tblexpense where (ExpenseDate)='$ydate' && (UserId='$userid');");
$result1=mysqli_fetch_array($query1);
$sum_yesterday_expense=$result1['yesterdayexpense'];
 ?>
                    <div class="panel-body easypiechart-panel">
                        <h4>Yesterday's Expense</h4>
                        <div class="easypiechart" id="easypiechart-orange" data-percent="<?php echo $sum_yesterday_expense;?>"><span class="percent">
				<?php if($sum_yesterday_expense==""){
                            echo "0";
                       } else {
                         echo $sum_yesterday_expense;
                     }
	?></span></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3">
                <div class="panel panel-default">
                    <?php
//Monthly Expense
$userid=$_SESSION['employeeid'];
 $monthdate=  date("Y-m-d", strtotime("-1 month")); 
$crrntdte=date("Y-m-d");
$query2=mysqli_query($con,"select sum(ExpenseCost)  as monthlyexpense from tblexpense where ((ExpenseDate) between '$monthdate' and '$crrntdte') && (UserId='$userid');");
$result2=mysqli_fetch_array($query2);
$sum_monthly_expense=$result2['monthlyexpense'];
 ?>
                    <div class="panel-body easypiechart-panel">
                        <h4>Last 30day's Expenses</h4>
                        <div class="easypiechart" id="easypiechart-red"
                            data-percent="<?php echo $sum_monthly_expense;?>"><span class="percent"><?php if($sum_monthly_expense==""){
echo "0";
} else {
echo $sum_monthly_expense;
}

	?></span></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3">
                <div class="panel panel-default">
                    <?php
//Yearly Expense
$userid=$_SESSION['employeeid'];
$query3=mysqli_query($con,"select sum(ExpenseCost)  as totalexpense from tblexpense where UserId='$userid';");
$result3=mysqli_fetch_array($query3);
$sum_total_expense=$result3['totalexpense'];
 ?>
                    <div class="panel-body easypiechart-panel">
                        <h4>Total Expenses</h4>
                        <div class="easypiechart" id="easypiechart-red" data-percent="<?php echo $sum_total_expense;?>">
                            <span class="percent"><?php if($sum_total_expense==""){
echo "0";
} else {
echo $sum_total_expense;
}

	?></span></div>
                    </div>
                </div>
            </div>

        </div>

<!-- this this the second row concerning sales -->

        <div class="row">
            <div class="col-xs-6 col-md-3">

                <div class="panel panel-default">
                    <div class="panel-body easypiechart-panel">
                        <?php
//Today Sales 
$userid=$_SESSION['employeeid'];
$tdate=date('Y-m-d');
$query4=mysqli_query($con,"select sum(SalePrice)  as todaysale from tblsale where (SaleDate)='$tdate' && (UserId='$userid');");
$result4=mysqli_fetch_array($query4);
$sum_today_sale=$result4['todaysale'];
 ?>

                        <h4>Today's Sale</h4>
                        <div class="easypiechart" id="easypiechart-blue"
                            data-percent="<?php echo $sum_today_sale;?>"><span class="percent"><?php if($sum_today_sale==""){
echo "0";
} else {
echo $sum_today_expense;
}

	?></span></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3">
                <div class="panel panel-default">
                    <?php
//Yestreday Sales
$userid=$_SESSION['employeeid'];
$ydate=date('Y-m-d',strtotime("-1 days"));
$query5=mysqli_query($con,"select sum(SalePrice)  as yesterdaysale from tblsale where (SaleDate)='$ydate' && (UserId='$userid');");
$result5=mysqli_fetch_array($query5);
$sum_yesterday_sale=$result5['yesterdaysale'];
 ?>
                    <div class="panel-body easypiechart-panel">
                        <h4>Yesterday's Sale</h4>
                        <div class="easypiechart" id="easypiechart-orange" data-percent="<?php echo $sum_yesterday_sale;?>"><span class="percent">
				<?php if($sum_yesterday_sale==""){
                            echo "0";
                       } else {
                         echo $sum_yesterday_sale;
                     }
	?></span></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3">
                <div class="panel panel-default">
                    <?php
//Monthly Sales
$userid=$_SESSION['employeeid'];
 $monthdate=  date("Y-m-d", strtotime("-1 month")); 
$crrntdte=date("Y-m-d");
$query6=mysqli_query($con,"select sum(SalePrice) as monthlysale from tblsale where ((SaleDate) between '$monthdate' and '$crrntdte') && (UserId='$userid');");
$result6=mysqli_fetch_array($query6);
$sum_monthly_sale=$result6['monthlysale'];
 ?>
                    <div class="panel-body easypiechart-panel">
                        <h4>Last 30day's Sales</h4>
                        <div class="easypiechart" id="easypiechart-red"
                            data-percent="<?php echo $sum_monthly_sale;?>"><span class="percent"><?php if($sum_monthly_sale==""){
echo "0";
} else {
echo $sum_monthly_sale;
}

	?></span></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3">
                <div class="panel panel-default">
                    <?php
//Yearly Sales
$userid=$_SESSION['employeeid'];
$query7=mysqli_query($con,"select sum(SalePrice)  as totalsale from tblsale where UserId='$userid';");
$result7=mysqli_fetch_array($query7);
$sum_total_expense=$result7['totalsale'];
 ?>
                    <div class="panel-body easypiechart-panel">
                        <h4>Total Sales</h4>
                        <div class="easypiechart" id="easypiechart-red" data-percent="<?php echo $sum_total_sale;?>">
                            <span class="percent"><?php if($sum_total_sale==""){
echo "0";
} else {
echo $sum_total_sale;
}

	?></span></div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!--/.main-->
    <?php include_once('includes/footer.php');?>
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/chart.min.js"></script>
    <script src="js/chart-data.js"></script>
    <script src="js/easypiechart.js"></script>
    <script src="js/easypiechart-data.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/custom.js"></script>
    <script>
    window.onload = function() {
        var chart1 = document.getElementById("line-chart").getContext("2d");
        window.myLine = new Chart(chart1).Line(lineChartData, {
            responsive: true,
            scaleLineColor: "rgba(0,0,0,.2)",
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleFontColor: "#c5c7cc"
        });
    };
    </script>

</body>

</html>
<?php } ?>