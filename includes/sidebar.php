<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
?>


<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
        <div class="profile-sidebar">
            <div class="profile-userpic">
                <img src="iss.png" class="img-responsive" alt="">
            </div>
            <div class="profile-usertitle">
                <?php
        $uid=$_SESSION['employeeid'];
        $ret=mysqli_query($con,"select FullName from tbluser where ID='$uid'");
        $row=mysqli_fetch_array($ret);
        $name=$row['FullName'];

?>
                <div class="profile-usertitle-name"><?php echo $name; ?></div>
                <h6 style="color: #30a5ff;">Employee</h6>
            </div>
            <div class="clear"></div>
        </div>
        <div class="divider"></div>
        
        <ul class="nav menu">
            <li class="active"><a href="dashboard.php"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>

           
            <li class="parent "><a data-toggle="collapse" href="#sub-item-1">
                <em class="fa fa-navicon">&nbsp;</em>Expenses <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
                </a>
                <ul class="children collapse" id="sub-item-1">
                    <li><a class="" href="add-expense.php">
                        <span class="fa fa-plus-circle">&nbsp;</span> Add Expense
                    </a></li>
                    <li><a class="" href="view-expense.php">
                        <span class="fa fa-eye">&nbsp;</span> View Expenses
                    </a></li>                   
                </ul>
            </li>
           
  <li class="parent "><a data-toggle="collapse" href="#sub-item-2">
                <em class="fa fa-navicon">&nbsp;</em>Sales <span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
                </a>
                <ul class="children collapse" id="sub-item-2">
                    <li><a class="" href="add-sale.php">
                        <span class="fa fa-plus-circle">&nbsp;</span> Add Sale
                    </a></li>
                    <li><a class="" href="view-sale.php">
                        <span class="fa fa-eye">&nbsp;</span> View Sales
                    </a></li>                 
                </ul>
            </li>

            <li class="parent "><a data-toggle="collapse" href="#sub-item-3">
                <em class="fa fa-navicon">&nbsp;</em> Client Request <span data-toggle="collapse" href="#sub-item-3" class="icon pull-right"><em class="fa fa-plus"></em></span>
                </a>
                <ul class="children collapse" id="sub-item-3">
                    <li><a class="" href="add-service.php">
                        <span class="fa fa-plus-circle">&nbsp;</span> New Request
                    </a></li>
                    <li><a class="" href="view-service.php">
                        <span class="fa fa-eye">&nbsp;</span> View Requests
                    </a></li>                   
                </ul>
            </li>

            <li class="parent "><a data-toggle="collapse" href="#sub-item-4">
                <em class="fa fa-navicon">&nbsp;</em> Interns <span data-toggle="collapse" href="#sub-item-4" class="icon pull-right"><em class="fa fa-plus"></em></span>
                </a>
                <ul class="children collapse" id="sub-item-4">
                    <li><a class="" href="add-intern.php">
                        <span class="fa fa-plus-circle">&nbsp;</span> New Intern
                    </a></li>
                    <li><a class="" href="view-intern.php">
                        <span class="fa fa-eye">&nbsp;</span> View Intern
                    </a></li>                   
                </ul>
            </li>

            <li class="parent "><a data-toggle="collapse" href="#sub-item-5">
                <em class="fa fa-user">&nbsp;</em> My Account <span data-toggle="collapse" href="#sub-item-5" class="icon pull-right"><em class="fa fa-plus"></em></span>
                </a>
                <ul class="children collapse" id="sub-item-5">
                    <li><a class="" href="user-profile.php">
                        <span class="fa fa-user-circle">&nbsp;</span> Profile
                    </a></li>
                    <li><a class="" href="change-password.php">
                        <span class="fa fa-clone">&nbsp;</span> Change Password
                    </a></li>  
                    <li><a class="" href="logout.php">
                        <span class="fa fa-power-off">&nbsp;</span> Logout
                    </a></li>                  
                </ul>
            </li>  

            <li><a href="make-repport.php">
                <em class="fa fa-user">&nbsp;</em> Make Repport <span data-toggle="collapse" href="#sub-item-5"></span>
                </a>
            </li>           

        </ul>
    </div>