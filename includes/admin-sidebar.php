<?php
   session_start();
     error_reporting(0);
       include('includes/dbconnection.php');
?>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
        <div class="profile-sidebar">
            <div class="profile-userpic">
                <img src="iss.png" class="img-responsive" alt="">
            </div>
            <div class="profile-usertitle">
        <?php
             $uid=$_SESSION['adminid'];
             $ret=mysqli_query($con,"select Username from tbladmin where ID='$uid'");
             $row=mysqli_fetch_array($ret);
            $name=$row['Username'];
?>
                <div class="profile-usertitle-name"><?php echo $name; ?></div>
                <h6 style="color: #30a5ff;">Admin</h6>
            </div>
            <div class="clear"></div>
        </div>
        <div class="divider"></div>
        
        <ul class="nav menu">
            <li class="active"><a href="admin-dashboard.php"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
     
            <li class="parent "><a data-toggle="collapse" href="#sub-item-1">
                <em class="fa fa-navicon">&nbsp;</em>Expenses <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
                </a>
                <ul class="children collapse" id="sub-item-1">
                    <li><a class="" href="manage-expense.php">
                        <span class="fa fa-cogs">&nbsp;</span> Manage Expenses
                    </a></li>
                    <li><a class="" href="admin-dashboard.php">
                        <span class="fa fa-bar-chart">&nbsp;</span> View Expense Chart
                    </a></li>                    
                </ul>

            </li>
           
  <li class="parent "><a data-toggle="collapse" href="#sub-item-2">
                <em class="fa fa-navicon">&nbsp;</em>Sales <span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
                </a>
                <ul class="children collapse" id="sub-item-2">
                    <li><a class="" href="manage-sale.php">
                        <span class="fa fa-cogs">&nbsp;</span> Manage Sales
                    </a></li>
                    <li><a class="" href="admin-dashboard.php">
                        <span class="fa fa-bar-chart">&nbsp;</span> View Sales Chart
                    </a></li>
                </ul>
            </li>

            <li class="parent "><a data-toggle="collapse" href="#sub-item-3">
                <em class="fa fa-navicon">&nbsp;</em>Services <span data-toggle="collapse" href="#sub-item-3" class="icon pull-right"><em class="fa fa-plus"></em></span>
                </a>
                <ul class="children collapse" id="sub-item-3">
                    <li><a class="" href="manage-service.php">
                        <span class="fa fa-cogs">&nbsp;</span> Manage Requests
                    </a></li>
                    <li><a class="" href="#">
                        <span class="fa fa-calendar">&nbsp;</span> Set Shedule
                    </a></li>                   
                </ul>
            </li>

            <li class="parent "><a data-toggle="collapse" href="#sub-item-4">
                <em class="fa fa-users">&nbsp;</em>Employee <span data-toggle="collapse" href="#sub-item-4" class="icon pull-right"><em class="fa fa-plus"></em></span>
                </a>
                <ul class="children collapse" id="sub-item-4">
                    <li><a class="" href="manage-employee.php">
                        <span class="fa fa-eye">&nbsp;</span> Manage Employees
                    </a></li>  
                    <li><a class="" href="manage-repport.php">
                        <span class="fa fa-cogs">&nbsp;</span> Manage Repport     
                    </a></li>                  
                </ul>
            </li>

            <li class="parent "><a data-toggle="collapse" href="#sub-item-5">
                <em class="fa fa-users">&nbsp;</em>Intern <span data-toggle="collapse" href="#sub-item-5" class="icon pull-right"><em class="fa fa-plus"></em></span>
                </a>
                <ul class="children collapse" id="sub-item-5">
                    <li><a class="" href="manage-employee.php">
                        <span class="fa fa-eye">&nbsp;</span> Manage Intern
                    </a></li>  
                    <li><a class="" href="manage-repport.php">
                        <span class="fa fa-cogs">&nbsp;</span> Projects    
                    </a></li>                  
                </ul>
            </li>

            <li class="parent "><a data-toggle="collapse" href="#sub-item-6">
                <em class="fa fa-user">&nbsp;</em> My Account <span data-toggle="collapse" href="#sub-item-6" class="icon pull-right"><em class="fa fa-plus"></em></span>
                </a>
                <ul class="children collapse" id="sub-item-6">
                    <li><a class="" href="admin-change-password.php">
                        <span class="fa fa-clone">&nbsp;</span> Change Password
                    </a></li>  
                    <li><a class="" href="logout.php">
                        <span class="fa fa-power-off">&nbsp;</span> Logout
                    </a></li>                  
                </ul>
            </li>

        </ul>
    </div>