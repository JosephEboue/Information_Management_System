<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
error_reporting(0);

if(isset($_POST['submit']))
  {
    $contactno=$_SESSION['contactno'];
    $email=$_SESSION['email'];
    $password=md5($_POST['newpassword']);

        $query=mysqli_query($con,"update tbluser set Password='$password'  where  Email='$email' && MobileNumber='$contactno' ");
   if($query)
   {
echo "<script>alert('Password successfully changed');</script>";
session_destroy();
   }
  
  }
  ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ISS IMS - Forgot Reset</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	<script type="text/javascript">

function checkpass(){
    if(document.changepassword.newpassword.value!=document.changepassword.confirmpassword.value){
        alert('New Password and Confirm Password field does not match');
          document.changepassword.confirmpassword.focus();
             return false;
       }
         return true;
} 

</script>
</head>
<body>
     <div class="row">
	   <center><img src="iss.png" class="img-responsive" alt="ISS logo" width="100px" height="100px"></center>
	</div>

	<div class="row">
			<h2 align="center"><b>ISS IMS</b></h2>
	<hr />
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading"><b>Reset Password</b></div>
				<div class="panel-body">
					<p style="font-size:16px; color:red" align="center"> <?php if($msg){ echo $msg;}  ?> </p>
					<form role="form" action="" method="post" name="changepassword" onsubmit="return checkpass()">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="newpassword" type="password" value="" required="true">
							</div>
							
							<div class="form-group">
								<input class="form-control" placeholder="Confirm Password" name="confirmpassword" type="password" value="" required="true">
							</div>
							<div class="form-group">
								<input class="form-control btn btn-primary" name="submit" type="submit" value="Reset">
							</div>
							<div class="checkbox">
								<center><a href="index.php" style="color: #337ab7;"><b>Login</b></a></span></center>
							</div>
						
							</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div><	
	

<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
