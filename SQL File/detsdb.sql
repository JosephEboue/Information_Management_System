-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 02, 2021 at 02:43 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `detsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbladmin`
--

CREATE TABLE `tbladmin` (
  `Username` varchar(15) NOT NULL,
  `Password` varchar(20) NOT NULL,
  `ID` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbladmin`
--

INSERT INTO `tbladmin` (`Username`, `Password`, `ID`) VALUES
('Admin', 'Admin12345', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblexpense`
--

CREATE TABLE `tblexpense` (
  `ID` int(10) NOT NULL,
  `UserId` int(10) NOT NULL,
  `ExpenseDate` date DEFAULT NULL,
  `ExpenseItem` varchar(200) DEFAULT NULL,
  `ExpenseCost` varchar(200) DEFAULT NULL,
  `NoteDate` timestamp NULL DEFAULT current_timestamp(),
  `Motif` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblexpense`
--

INSERT INTO `tblexpense` (`ID`, `UserId`, `ExpenseDate`, `ExpenseItem`, `ExpenseCost`, `NoteDate`, `Motif`) VALUES
(9, 2, '2019-05-08', 'Bed Sheets', '890', '2019-05-15 10:08:57', ''),
(31, 10, '2019-05-16', 'Computer Mouse', '500', '2019-05-18 05:35:45', ''),
(38, 11, '2021-03-22', 'Detergent', '500', '2021-03-22 22:26:13', 'To make general cleaning in the enterprise'),
(39, 11, '2021-03-23', 'A4 Papers', '7500', '2021-03-23 12:34:24', 'To replace in the printer'),
(40, 11, '2021-03-23', 'Data Connection', '10000', '2021-03-23 18:53:05', 'To made researches'),
(41, 12, '2021-03-24', 'keyboard', '1500', '2021-03-24 12:10:21', 'old keyboard was bad'),
(42, 11, '2021-03-27', 'Printer ink', '7500', '2021-03-27 10:37:37', 'printer ink was finished');

-- --------------------------------------------------------

--
-- Table structure for table `tblintern`
--

CREATE TABLE `tblintern` (
  `ID` int(2) NOT NULL,
  `InternName` varchar(50) NOT NULL,
  `InternPhone` int(10) NOT NULL,
  `InternEmail` varchar(30) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblintern`
--

INSERT INTO `tblintern` (`ID`, `InternName`, `InternPhone`, `InternEmail`, `start`, `end`) VALUES
(1, 'EBOUE BEBEY JOSEPH AIGLON', 655195030, 'baiglon9@gmail.com', '2020-08-12', '2020-09-26'),
(2, 'CHICHE BRADON AYUK', 671283638, 'chichebradon@gmail.com', '2020-08-12', '2020-09-19');

-- --------------------------------------------------------

--
-- Table structure for table `tblrepport`
--

CREATE TABLE `tblrepport` (
  `Id` int(10) NOT NULL,
  `EmpName` varchar(30) NOT NULL,
  `Repport` varchar(500) NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblrepport`
--

INSERT INTO `tblrepport` (`Id`, `EmpName`, `Repport`, `Date`) VALUES
(1, 'mercy', 'We had problems today with tax payment', '2021-03-27');

-- --------------------------------------------------------

--
-- Table structure for table `tblsale`
--

CREATE TABLE `tblsale` (
  `ID` int(10) NOT NULL,
  `UserId` int(3) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `SaleItem` varchar(50) NOT NULL,
  `SalePrice` varchar(30) NOT NULL,
  `SaleDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `Customer` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblsale`
--

INSERT INTO `tblsale` (`ID`, `UserId`, `Quantity`, `SaleItem`, `SalePrice`, `SaleDate`, `Customer`) VALUES
(2, 11, 20220321, 'keyboard', '1500', '2021-03-22 22:04:56', 'Gilberto'),
(4, 11, 20220321, 'USB Flash', '3000', '2021-03-22 22:09:05', 'marie monro'),
(5, 11, 20220321, 'Computer elite book', '250000', '2021-03-22 22:09:51', 'Steven Luis'),
(6, 11, 20230321, 'RJ45 Cable', '2000', '2021-03-23 18:55:06', 'Romeo simo'),
(7, 12, 1, 'canal + plateau', '25000', '2021-03-24 12:22:09', 'zane'),
(8, 11, 1, 'mobile phone', '80000', '2021-03-27 10:34:48', ' Joseph');

-- --------------------------------------------------------

--
-- Table structure for table `tblservice`
--

CREATE TABLE `tblservice` (
  `ID` int(5) NOT NULL,
  `ServiceName` varchar(30) NOT NULL,
  `UserId` int(10) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `CustomerName` varchar(30) NOT NULL,
  `CustomerContact` bigint(15) NOT NULL,
  `ServiceDetails` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblservice`
--

INSERT INTO `tblservice` (`ID`, `ServiceName`, `UserId`, `Date`, `CustomerName`, `CustomerContact`, `ServiceDetails`) VALUES
(1, 'Web Application', 11, '2021-03-23 10:36:08', 'Mr koum Cabrel', 671938328, 'Development of a an e-commerce web app which will permit Mr Cabrel to manage his business'),
(2, 'Mobile App', 11, '2021-03-23 18:44:12', 'Herman', 677330823, 'For an e-commerce');

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

CREATE TABLE `tbluser` (
  `ID` int(10) NOT NULL,
  `FullName` varchar(150) DEFAULT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `MobileNumber` bigint(10) DEFAULT NULL,
  `Password` varchar(200) DEFAULT NULL,
  `RegDate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbluser`
--

INSERT INTO `tbluser` (`ID`, `FullName`, `Email`, `MobileNumber`, `Password`, `RegDate`) VALUES
(10, 'Test User demo', 'testuser@gmail.com', 987654321, 'd8578edf8458ce06fbc5bb76a58c5ca4', '2019-05-18 05:34:53'),
(11, 'JOSEPH AIGLON', 'baiglon9@gmail.com', 6655195030, 'bd9fa9edbeff8f0b88a6f26ce7665953', '2021-03-20 16:25:06'),
(12, 'Mercy', 'mercy@gmail.com', 1234567890, 'bf2ff2ed3c83c3c5ce510c4666f6fb0d', '2021-03-24 12:04:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbladmin`
--
ALTER TABLE `tbladmin`
  ADD PRIMARY KEY (`ID`,`Password`,`Username`);

--
-- Indexes for table `tblexpense`
--
ALTER TABLE `tblexpense`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblintern`
--
ALTER TABLE `tblintern`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblrepport`
--
ALTER TABLE `tblrepport`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tblsale`
--
ALTER TABLE `tblsale`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblservice`
--
ALTER TABLE `tblservice`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbluser`
--
ALTER TABLE `tbluser`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbladmin`
--
ALTER TABLE `tbladmin`
  MODIFY `ID` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblexpense`
--
ALTER TABLE `tblexpense`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `tblintern`
--
ALTER TABLE `tblintern`
  MODIFY `ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tblrepport`
--
ALTER TABLE `tblrepport`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblsale`
--
ALTER TABLE `tblsale`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tblservice`
--
ALTER TABLE `tblservice`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbluser`
--
ALTER TABLE `tbluser`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
