<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');

if(isset($_POST['login'])){
    $name=$_POST['username'];
    $password=$_POST['password'];
    $query=mysqli_query($con,"select ID from tbladmin where  Username='$name' && Password='$password'");
    $ret=mysqli_fetch_array($query);
    if($ret>0){
      $_SESSION['adminid']=$ret['ID'];
     header('location:admin-dashboard.php');
    }
    else{
    $msg="Invalid Details.";
    }
  }
  ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ISS IMS - Login</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
</head>
<body>
	     <div class="row">
	  <center><img src="iss.png" class="img-responsive" alt="ISS logo" width="100px" height="100px"></center>
		</div>
		
	<div class="row">
    <div class="checkbox">
			<h2 align="center"><a href="index.php"><b> I M S</b></a></h2>
			</div>
	<hr />
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading"><b>Admin Login</b></div>
				<div class="panel-body">
					<p style="font-size:16px; color:red" align="center"> <?php if($msg){echo $msg;}  ?> </p>
					<form role="form" action="" method="post" name="login">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="Username" name="username" type="text" autofocus="" required="true">
							</div>

							<div class="form-group">
								<input class="form-control" placeholder="Password" name="password" type="password" value="" required="true">
							</div>
							<div class="form-group">
								<input class="form-control btn btn-primary" name="login" type="submit" value="Sign-in">
							</div>

							</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>	
	

<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
