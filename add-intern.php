<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['employeeid']==0)) {
  header('location:logout.php');
  } else{
 
    // Insert interns details into the database (in table `tblintern`)

if(isset($_POST['submit'])){
  //	$userid=$_SESSION['employeeid'];
    $iname=$_POST['internname'];
     $iphone=$_POST['internphone'];
     $iemail=$_POST['internemail'];
     $istart=$_POST['start'];
     $iend=$_POST['end'];
    $query=mysqli_query($con, "insert into tblintern(InternName,InternPhone,InternEmail,start,end) value('$iname',$iphone,'$iemail','$istart','$iend')");
if($query){
echo "<script>alert('Added successfully');</script>";
// echo "<script>window.location.href='view-service'</script>";
} else {
    echo "<center>";
    echo "Error: " . $query . "<br>" . $con->error;
    echo "</center>";
//echo "<script>alert('Something went wrong. Service not added, please try again');</script>";
}
  
}
  ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ISS IMS || New Service</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/datepicker3.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">

    <!--Custom Font-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

</head>

<body>
    <?php include_once('includes/header.php');?>
    <?php include_once('includes/sidebar.php');?>

    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="dashboard.php">
                        <em class="fa fa-home"></em>
                    </a></li>
                <li class="active">Add New Intern</li>
            </ol>
        </div>

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">
                    <div class="panel-heading">New Intern</div>
                    <div class="panel-body">
                        <p style="font-size:16px; color:red" align="center"> <?php if($msg){
                            echo $msg;}  ?> </p>
                        <div class="col-md-12">

                            <form role="form" method="post" action="">
                                <div class="form-group">
                                    <label>Fullname</label>
                                    <input class="form-control" name="internname" type="text" value="" required="true">
                                </div>
                                <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Phone Number</label>
                                    <input type="int" class="form-control" name="internphone" value="" required="true">
                                </div>

                                <div class="form-group col-md-6">
                                    <label>E-mail</label>
                                    <input class="form-control" type="email" value="" required="true" name="internemail">
                                </div></div>

                                  <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Start of Internship</label>
                                    <input class="form-control" type="date" value="" required="false" name="start">

                                </div>

                                <div class="form-group col-md-6">
                                    <label>End of internship</label>
                                    <input class="form-control" type="date" value="" required="false" name="end">
                                </div></div>

                                <div class="form-group has-success">
                                    <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                                </div>


                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('includes/footer.php');?>
    </div>
    </div>

    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/chart.min.js"></script>
    <script src="js/chart-data.js"></script>
    <script src="js/easypiechart.js"></script>
    <script src="js/easypiechart-data.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/custom.js"></script>

</body>

</html>
<?php }  ?>