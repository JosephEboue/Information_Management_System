<?php  
 session_start();
  error_reporting(0);
   include('includes/dbconnection.php');
    if (strlen($_SESSION['employeeid']==0)) {
        header('location:logout.php');
    } else{
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ISS IMS || Manage Expense</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

</head>
<body>
	<?php include_once('includes/header.php');?>
	<?php include_once('includes/sidebar.php');?>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="dashboard.php">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">View Interns</li>
			</ol>
		</div>

		<div class="row">
			<div class="col-lg-12">

				<div class="panel panel-default">
					<div class="panel-heading">View Interns</div>
					<div class="panel-body">
						<p style="font-size:16px; color:red" align="center"> <?php if($msg){
                           echo $msg;}  ?> </p>
						<div class="col-md-12">
							
							<div class="table-responsive">
            <table class="table table-bordered mg-b-0">
              <thead>
                <tr>
                  <th>S.NO</th>
                  <th>FullName</th>
                  <th>Contact</th>
                  <th>Email</th>
                  <th>Start</th>
                  <th>End</th>
                </tr>
              </thead>
<?php
      //  $userid=$_SESSION['employeeid'];
        $ret=mysqli_query($con,"select * from tblintern");
        $cnt=1;
           while ($row=mysqli_fetch_array($ret)) {
?>
              <tbody>
                <tr>
                  <td><?php echo $cnt;?></td>              
                  <td><?php  echo $row['InternName'];?></td>
                  <td><?php  echo $row['InternPhone'];?></td>
                  <td><?php  echo $row['InternEmail'];?></td> 
                  <td><?php  echo $row['start'];?></td>                   
                  <td><?php  echo $row['end'];?></td>                 
                </tr>
                <?php $cnt=$cnt+1; }?>
               
              </tbody>
            </table>
          </div>
						</div>
					</div>
				</div>
			</div>
			<?php include_once('includes/footer.php');?>
		</div>
	</div>
	
<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	
</body>
</html>
<?php }  ?>