<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['employeeid']==0)) {
  header('location:logout.php');
  } else{

// PHP code to add sales into database
if(isset($_POST['submit'])){
  	$userid=$_SESSION['employeeid'];
     $item=$_POST['item'];
     $quantity=$_POST['quantity'];
     $price=$_POST['itemprice'];
     $customer=$_POST['customer'];
    $query=mysqli_query($con, "insert into tblsale(UserId,SaleItem,Quantity,SalePrice,Customer) value('$userid','$item',$quantity,'$price','$customer')");
if($query){
echo "<script>alert('Sale added successfully');</script>";
// echo "<script>window.location.href='manage-sale.php'</script>";
} else {
echo "<script>alert('Something went wrong. Unable to add sale, please try again');</script>";

}
  
}
  ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ISS IMS || Add Sale</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/datepicker3.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">

    <!--Custom Font-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

</head>

    <!--Add Sales HTML form-->
<body>
    <?php include_once('includes/header.php');?>
    <?php include_once('includes/sidebar.php');?>

    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="dashboard.php">
                        <em class="fa fa-home"></em>
                    </a></li>
                <li class="active">Sale</li>
            </ol>
        </div>

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">
                    <div class="panel-heading">New Sale</div>
                    <div class="panel-body">
                        <p style="font-size:16px; color:red" align="center"> <?php if($msg){
                            echo $msg;}  ?> </p>
                        <div class="col-md-12">

                            <form role="form" method="post" action="">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label>Item</label>
                                    <input type="text" class="form-control" name="item" value="" required="true">
                                </div>

                                <div class="form-group col-md-4">
                                    <label>Quantity</label>
                                    <input class="form-control" name="quantity" type="int" value="" required="true">
                                </div>

                                <div class="form-group col-md-4">
                                    <label>Price</label>
                                    <input class="form-control" type="text" value="" required="true" name="itemprice">
                                </div></div>

                                <div class="form-group">
                                    <label>Customer Name</label>
                                    <input class="form-control" type="text" value="" required="true" name="customer">
                                </div>

                                <div class="form-group has-success">
                                    <button type="submit" class="btn btn-primary" name="submit">Add</button>
                                </div>


                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once('includes/footer.php');?>
    </div>
    </div>
    
<!--Bootstrap javascript templates -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/chart.min.js"></script>
    <script src="js/chart-data.js"></script>
    <script src="js/easypiechart.js"></script>
    <script src="js/easypiechart-data.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/custom.js"></script>

</body>

</html>
<?php }  ?>